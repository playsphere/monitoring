#
# docker-compose.yml
#
# Config file for running the following images
# - elasticsearch (master, loadbalancer and data node)
# - logstash
# - kibana
#
# To use elasticsearch in a cluster use the docker-compose scale logic:
# docker-compose scale elasticsearch=3
# Elasticsearch config use zen for instance discovering.
# Consider that a master and loadbalancer node is also configured. Kibana and
# Logstash instance should never act directly with a master node. For this reason
# is on the master node http.enabled deactivated.
#
#
# Hafid Haddouti <code@haddouti.com>
# 23.05.2017
# 

version: '3'

services:

  # Coordinating/Loadbalancing node
  # master, data and ingest features are deactivated
  # static port mapping for usage with external services like Kibana, Logstash
  elasticsearch-coordinating:
    build: elasticsearch/
    container_name: elasticsearch-coordinating
    volumes:
      - ./elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - "9200:9200"
      - "9300:9300"
    environment:
      - "ES_JAVA_OPTS=-Xmx256m -Xms256m"
      - "node.master=false"
      - "node.data=false"
      - "node.ingest=false"
      - "discovery.type=zen"
      - "discovery.zen.ping.unicast.hosts=elasticsearch-master"
      - "discovery.zen.minimum_master_nodes=1"
    networks:
      - elk

  # Master node
  # Without static port mapping and http transport disabled
  elasticsearch-master:
    build: elasticsearch/
#    container_name: elasticsearch-master
    volumes:
      - ./elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - "9300"
    environment:
      - "ES_JAVA_OPTS=-Xmx256m -Xms256m"
      - "node.master=true"
      - "node.data=false"
      - "node.ingest=false"
      - "http.enabled=false"
#      - "discovery.zen.minimum_master_nodes=2"
      - "discovery.zen.ping.unicast.hosts=elasticsearch-master"
    networks:
      - elk
    depends_on:
      - elasticsearch-coordinating


  # elasticsearch data service. 
  # Do not use a static container name, this will be generated during scaling
  # also do not use static port mapping, instead let elasticsearch use random ports
  elasticsearch:
    build: elasticsearch/
#    container_name: elasticsearch
    volumes:
      - ./elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - "9300"
    environment:
      - "ES_JAVA_OPTS=-Xmx256m -Xms256m"
      - "node.master=false"
      - "node.ingest=true"
      - "http.enabled=false"
      - "discovery.type=zen"
      - "discovery.zen.ping.unicast.hosts=elasticsearch-master"
#      - "discovery.zen.minimum_master_nodes=2"
    networks:
      - elk
    depends_on:
      - elasticsearch-coordinating

  logstash:
    build: logstash/
    container_name: logstash
    volumes:
      - ./logstash/config/logstash.yml:/usr/share/logstash/config/logstash.yml
      - ./logstash/pipeline:/usr/share/logstash/pipeline
    ports:
      - "5000:5000"
    environment:
      - "LS_JAVA_OPTS=-Xmx256m -Xms256m"
    networks:
      - elk
    links:
      - elasticsearch-coordinating
    depends_on:
      - elasticsearch-master
      - elasticsearch-coordinating

  kibana:
    build: kibana/
    container_name: kibana
    volumes:
      - ./kibana/config/kibana.yml:/usr/share/kibana/config/kibana.yml
    ports:
      - "5601:5601"
    networks:
      - elk
    links:
      - elasticsearch-coordinating
    depends_on:
      - elasticsearch-master
      - elasticsearch-coordinating

networks:

  elk:
    driver: bridge
